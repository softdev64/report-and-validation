package com.supanut.reportproject;

import java.util.List;

public class ArtistService {

    public List<ArtistReport> getArtistByTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(10);
    }

    public List<ArtistReport> getArtistByTotalPrice(String begin, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(begin, end, 10);
    }

}
