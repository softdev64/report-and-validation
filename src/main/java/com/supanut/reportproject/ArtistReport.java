package com.supanut.reportproject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ArtistReport {

    private int id;
    private String name;
    private int totalQuantity;
    private float totalPrice;

    public ArtistReport(int id, String name, int totalQuantity, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    public ArtistReport(String name, int totalQuantity, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    public ArtistReport() {
        this.id = -1;
        this.name = "";
        this.totalQuantity = 0;
        this.totalPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ArtistReport{" + "id=" + id + ", name=" + name + ", totalQuantity=" + totalQuantity + ", totalPrice=" + totalPrice + '}';
    }

    public static ArtistReport fromRS(ResultSet rs) {
        ArtistReport ar = new ArtistReport();
        try {
            ar.setId(rs.getInt("ArtistId"));
            ar.setName(rs.getString("Name"));
            ar.setTotalQuantity(rs.getInt("TotalQuantity"));
            ar.setTotalPrice(rs.getFloat("TotalPrice"));
        } catch (SQLException ex) {
            Logger.getLogger(ArtistReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ar;
    }
}
